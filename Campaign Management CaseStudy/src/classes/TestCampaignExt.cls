@isTest(SeeAllData=true)
private class TestCampaignExt {
    
    public static testMethod void myTest(){
        
        PageReference pageref = page.CampaignPitch;
        test.setCurrentPage(pageref);
               
        //Campaign campnew= new Campaign();
        
        Campaign campnew = [select Name, Budget__c,Language__c,Publication_Cycle__c, Region__c from campaign where name='TestclassCampaign'];
        ApexPages.currentPage().getParameters().put('a', String.valueof(campnew.Language__c));
        ApexPages.currentPage().getParameters().put('b', String.valueOf(campnew.Publication_Cycle__c));
        ApexPages.currentPage().getParameters().put('c', String.valueOf(campnew.Region__c));
        ApexPages.currentPage().getParameters().put('d', String.valueOf(campnew.Budget__c));
        ApexPages.currentPage().getParameters().put('f', String.valueOf(campnew.Id));
     /*
        campnew.Language__c = 'English';
        campnew.Publication_Cycle__c = '2 weeks';
        campnew.Region__c = 'North';
        campnew.Budget__c = 1000;
        campnew.Name ='BJP Election Campaign' ;
        campnew.status='Planned';
        insert campnew;
       */ 
        /*con.language = campnew.Language__c;
        con.pubCycle = campnew.Publication_Cycle__c;
        con.region = campnew.Region__c;
        con.cBudget = integer.valueof(campnew.Budget__c);
        con.cname =campnew.Name ;
        con.cid = campnew.Id;
        */
       // con.publicationlist x = new publicationlist();
        //x.checked = true;
            
        List<publication__c> pubsT = new List<publication__c>();
        publication__c pubT = new publication__c();
        pubT.name = 'Times of India';
        pubT.Language__c = 'English';
        pubT.Region__c = 'North'; 
		pubT.Circulation__c = 300; 
        pubT.Publication_Cycle__c = '2 weeks';
        pubT.Publication_Fee__c = 500;
        pubT.Popularity__c = '4';
        pubsT.add(pubT);
        insert pubsT;
        
        Test.StartTest();          
 		
  		/* list<PublicationWrapperTest> wrapList= new  list<PublicationWrapperTest>();
  		  list<Publication__c> SelectedPublicationsTest = new  list<Publication__c>();
  		 for(Publication__c pubs : [Select Name, Publication_Fee__c,Language__c,Publication_Cycle__C, Region__c 
                From Publication__c 
                Where Language__c= :campnew.Language__c AND Publication_Cycle__c= :campnew.Publication_Cycle__c AND Region__c=:campnew.Region__c  AND Publication_fee__c<= :campnew.Budget__c])
            {
                wrapList.add(new PublicationWrapperTest(pubs));
            }
            */
  		ApexPages.StandardController sc = new ApexPages.StandardController(campnew);
	  //WrapperExample WE= new WrapperExample();
	  CampaignExt con = new CampaignExt();
  	/*for(PublicationWrapperTest WATest :wrapList)
        
  		{
   			WATest.checked=true;    
  		}*/
            for (CampaignExt.PublicationWrapper pw : con.publicationList) 
    		{
            pw.checked=true; 
    		}
  		con.publish();
        con.campaigns_page();
        con.checkWhetherItsValid();
  		Test.StopTest();
    }
    
   /* 
    public class PublicationWrapperTest
    {
        public Publication__c pbList {get; set;}
        public Boolean checked {get; set;}
        public PublicationWrapperTest(Publication__c a) 
        {
            pbList = a;
            checked = false;
        }     
    }
    */

}