@IsTest
public class CampaignStatusChangeTest {
    	static testMethod void StatusTrigTestMethod()
        {
            //creating new client
    Account TrigTestAcc = new Account(Name='TrigTestClient', Client_Rating__c= 'Very Frequent', Current_Popularity__c= '1', Targeted_Popularity__c= '2', Current_Revenue__c=1080, Targeted_Revenue__C=10089, Targeted_Regions_of_Operation__c='North');
    insert TrigTestAcc;
            // Creating New Campaign        
    List <Campaign> camps = new List<Campaign>();
    Campaign uniquecampaign = new Campaign(Name='TriggerTestCamp',Publication_Cycle__c='2 weeks',Language__c='English',Budget__c= 2000, Region__c='North', Expected_number_of_Publications__c= 1, client_name__c= '0012v00002O9bo6AAB');
    uniquecampaign.Client_Name__c = TrigTestAcc.Id;
    camps.add(uniquecampaign);
    insert camps;
    test.startTest();
            update camps;
    
    List <Opportunity> optys = new List<Opportunity>();
    Opportunity uniqueopty = new Opportunity(Name='TrigTestOpty', StageName='Prospecting', CloseDate=  date.ValueOf('2019-07-12') );
    //Campaign x=[select Id from Campaign Where Name= 'TriggerTestCampaign'];
	uniqueopty.CampaignId=uniquecampaign.Id;
    optys.add(uniqueopty);
    insert optys;
    
            uniqueopty.StageName= 'Closed Won';
            update optys;
            test.stopTest();
        
      }
}