public with sharing class CampaignExt {
    public Boolean isValid{get;set;}
    public Boolean noPbs{get;set;}
    public String language = ApexPages.currentPage().getParameters().get('a');
    public String pubCycle = ApexPages.currentPage().getParameters().get('b');
    public String region = ApexPages.currentPage().getParameters().get('c');
    public Integer cBudget = integer.valueOf(ApexPages.currentPage().getParameters().get('d').remove(','));
	public String cname = ApexPages.currentPage().getParameters().get('e');
    public String cid = ApexPages.currentPage().getParameters().get('f');
    //Integer cUpBudget = integer.valueOf(ApexPages.currentPage().getParameters().get('g').remove(','));
    //Integer cexp= integer.valueOf(ApexPages.currentPage().getParameters().get('h'));
    public Campaign camp = [select Name, Client_Name__c, EndDate from campaign where Id=:cid]; 
    //public Integer budgetCalc {get;set;}
    public List<Publication__c> selectedPublications= new List<Publication__c>(); 
    public List<PublicationWrapper> publicationList
    {
        get
        {
            if (publicationList == null) 
            {
                publicationList = new List<PublicationWrapper>();
        for(Publication__c pubs : [Select Name, Publication_Fee__c 
                From Publication__c 
                Where Language__c= :language AND Publication_Cycle__c= :pubCycle AND region__c=:region  AND Publication_fee__c<= :cBudget AND ID NOT IN (select Publication_Name__c from Opportunity where CampaignId=:cid)])
            {
                publicationList.add(new PublicationWrapper(pubs));
            }
            }
            return publicationList;
        }
        set;
    }
    public PageReference publish()
    {
        //budgetCalc=0;
        for (PublicationWrapper pw : publicationList) {
            if (pw.checked) 
            {
                 //budgetCalc=budgetCalc+integer.valueOf(pw.pbList.Publication_Fee__c); 
                 selectedPublications.add(pw.pbList);
            }
        }
       
        
        
        // camp=[Select Name,Status,client_Name__c,enddate From Campaign where id=:cid];
       // List<Publication__c> relatedPublications = [select id from opportunity where Campaign_Name__c=:camp.id AND stage='Closed Won'];
		//relatedPublications.size();
        /* if(cUpBudget==0){
            cUpBudget=cBudget;
        }
        if(budgetCalc>cUpBudget){
            camp.status='In Progress';
        }
        else */
        // { 
            List<opportunity> lopportunities= new List<opportunity>();
            //camp.status='completed';    
            account accc = new account();
            accc = [select name from account where id=:camp.Client_Name__c];        
            for(Publication__c a:selectedPublications)
       		{
           		opportunity opp= new opportunity();
                opp.Publication_Name__c= a.id;
                
                opp.Name= accc.name+' '+a.Name+' opportunity';
                opp.AccountId= camp.client_name__c;
                opp.Amount= a.Publication_Fee__c;
                opp.CampaignId=camp.id;
                opp.CloseDate=camp.EndDate;
                opp.StageName='Prospecting';
                lopportunities.add(opp);      
                //a.Campaign_Name__c = camp.id;
           		//a.Published_Date__c = System.today();
        	}
         	insert lopportunities;
  			       
           // if(relatedPublications.size()+selectedPublications.size()>=cexp)
            //{
            //    camp.status='completed';
            //}
           // else
           // {
                camp.status='In Progress';
            //} 
        	//update selectedPublications; 
           // camp.Updated_Budget__c=cUpBudget-budgetCalc;
          
        //}
        update camp;
        PageReference campPage = new ApexPages.StandardController(camp).view();
        campPage.setRedirect(true);
        return campPage;
       
    }
    
    public PageReference campaigns_page(){
       /* Campaign camp= new Campaign();
        PageReference campPage = new ApexPages.StandardController(camp).view();
        campPage.setRedirect(true);
        return campPage; */
		PageReference pageRef = new PageReference('https://ap15.lightning.force.com/lightning/o/Campaign/list?filterName=00B2v00000E2ak5EAB');
		return pageRef;        

    }
    
    public class PublicationWrapper
    {
        public Publication__c pbList {get; set;}
        public Boolean checked {get; set;}
        public PublicationWrapper(Publication__c a) 
        {
            pbList = a;
            checked = false;
        }     
    }
    
    public void checkWhetherItsValid(){
        isValid = TRUE;
        noPbs=FALSE;
         camp=[Select Name,Status,client_Name__c,enddate From Campaign where id=:cid];
        if(camp.status=='completed'||camp.status=='aborted')
        {
            isValid = FALSE;
        }
        else{
            isValid = TRUE;
        }
        if(publicationList.size()==0)
        { noPbs=TRUE;}
        }
}