trigger CampaignStatusChange on Opportunity (after update) {
    
    List<Opportunity> opps = new List<Opportunity> ();
	List<Id> campaignIds = new List<Id>();
	for(Opportunity opp : trigger.new){
   		if(opp.StageName =='Closed Won'){
		opps.add(opp);
		campaignIds.add(opp.campaignid);
  		}
	}

List<Campaign> campaigns = [select id,Expected_number_of_Publications__c,budget__c,AmountWonOpportunities,NumberOfWonOpportunities,(select id,amount from Opportunities where StageName='Closed Won') from Campaign where ID IN :campaignIds];
for(Campaign campaign : campaigns){
     if(campaign.NumberOfWonOpportunities==campaign.Expected_number_of_Publications__c||campaign.AmountWonOpportunities>=campaign.Budget__c){
	 campaign.status='completed';
         system.debug('Triggered');
     }
     
}

update campaigns;

}